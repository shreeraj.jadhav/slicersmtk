//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.
 
  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/

#ifndef __vtkSlicerSMTKFileReaderLogic_h
#define __vtkSlicerSMTKFileReaderLogic_h

// SMTK includes
#include <smtk/resource/Resource.h>
#include <smtk/session/aeva/Session.h>

// Slicer includes
#include "vtkSlicerModuleLogic.h"

// STD includes
#include <string>
#include <vector>

// SMTKFileReader includes
#include "vtkSlicerSMTKFileReaderLogicExport.h"

namespace smtk { 
namespace session {
namespace aeva {
class Resource;
}
}
}

class vtkOrientedImageData;

/// \ingroup SlicerRt_QtModules_SMTKFileReader
class VTK_SLICER_SMTKFILEREADER_LOGIC_EXPORT vtkSlicerSMTKFileReaderLogic :
  public vtkSlicerModuleLogic
{
public:
  static vtkSlicerSMTKFileReaderLogic *New();
  vtkTypeMacro(vtkSlicerSMTKFileReaderLogic, vtkSlicerModuleLogic);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /// Read a smtk file and attempt to import it into the give vtkMRMLScene node
  /// \param scene The scene that we want to add data to
  /// \param smtkFileName the file that needs to be read w/o path
  /// \param smtkImportDir the path to the file to read
  /// \note smtk operations may throw, these exceptions aren't caught here so
  ///       this function may throw
  std::vector<std::string> ImportMRMLSceneFromAevaSMTKResources(
      vtkMRMLScene* scene, 
      const std::string& smtkFileName, 
      const std::string& smtkImportDir);

protected:
  vtkSlicerSMTKFileReaderLogic();
  ~vtkSlicerSMTKFileReaderLogic() override;

private:
  vtkSlicerSMTKFileReaderLogic(const vtkSlicerSMTKFileReaderLogic&) = delete;
  void operator=(const vtkSlicerSMTKFileReaderLogic&) = delete;

  /// Will try and find all known types in the resource and copy them to the scene
  /// \return The number of "issues" that have occurred (e.g. files that weren't found or others) 
  int CopyResourceToScene(vtkMRMLScene* scene, std::shared_ptr<smtk::session::aeva::Resource> resource);

  /// Convert an image data from aeva smtk scene to a MRML segmentation node.
  int createMRMLSegmentationNode(
    smtk::resource::ComponentPtr volComp,
    vtkOrientedImageData* imageFromSMTKScene,
    vtkMRMLScene* mrmlScene,
    const std::string& nameForNewMRMLNode);

  /// Convert an image data from aeva smtk scene to a MRML scalar volume node.
  int createMRMLScalarVolume(
    vtkOrientedImageData* imageFromSMTKScene,
    vtkMRMLScene* mrmlScene,
    const std::string& nameForNewMRMLNode);

  /// Convert a cell from aeva smtk scene to a MRML model volume node,
  /// which is either a surface (vtkPolyData) or a volumetric mesh (vtkUnstructuredGrid).
  int createMRMLModelVolume(
    smtk::model::CellEntity& cell,
    smtk::session::aeva::SessionPtr session,
    vtkMRMLScene* mrmlScene,
    const std::string& nameForNewMRMLNode);

  std::vector<std::string> m_loadedNodes;
  std::string m_importDir;
};

  /// Convenience function to get properties by-value (not reference).
  /// Helps in code brevity since we need to check for contains()
  /// every time we need to fetch a property value.
  /// \param component Input component object from which to get the property value.
  /// \param key Input key for fetching property value.
  /// \return Returns the value (not reference to it) based on template type T.
  template<typename T>
  T TryGetSMTKPropertyValue(smtk::resource::ComponentPtr component, const std::string& key, const T& defaultValue = {})
  {
    T propertyValue = defaultValue;
    if (component != nullptr)
    {
      if (component->properties().contains<T>(key))
      {
        propertyValue = component->properties().at<T>(key);
      }
    }

    return propertyValue;
  }

#endif
