//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Harald Scheirich, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK include
#include <vtkImageData.h>
#include <vtkXMLImageDataReader.h>
#include <vtkAddonTestingMacros.h>

// Slicer includes
#include <vtkMRMLNode.h>
#include <vtkMRMLScalarVolumeNode.h>
#include <vtkMRMLScene.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileReaderLogic.h>

// Std includes
#include <iostream>


int vtkSlicerSMTKFileReaderLogicImportImageSceneTest(int argc, char* argv[])
{
  bool test_success = true;
  std::string fileName =  "ImageScene.aeva.smtk";
  std::string scratchDir = ".";
  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/";
  if (argc >= 3)
  {
    scratchDir = std::string(argv[2]);
  }

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSMTKFileReaderLogic> logic;

  try
  {
    // Expecting one node that is an image node
    auto importedNodes = logic->ImportMRMLSceneFromAevaSMTKResources(scene, fileName, dataDir);

    CHECK_INT(importedNodes.size(), 1);
    CHECK_INT(scene->GetNumberOfNodes(), 1);
    auto node = scene->GetNthNode(0);
    CHECK_BOOL(std::string(node->GetName()) == importedNodes[0], true);
    auto typedNode = dynamic_cast<vtkMRMLScalarVolumeNode*>(node);
    CHECK_NOT_NULL(typedNode);

    vtkNew<vtkXMLImageDataReader> reader;

    auto imageFileName = dataDir + "ImageScene/" + "Volume_bbebee23-0054-4c5e-8e09-b9e70c34e3a5.vti";

    reader->SetFileName(imageFileName.c_str());
    reader->Update();

    auto image = reader->GetOutput();
    auto imageOrigin = image->GetOrigin();
    auto nodeOrigin = typedNode->GetOrigin();

    auto imageSpacing = image->GetSpacing();
    auto nodeSpacing = typedNode->GetSpacing();

    for (int i = 0; i < 3; ++i)
    {
      CHECK_DOUBLE_TOLERANCE(imageOrigin[i], nodeOrigin[i], 1.0e-4);
      CHECK_DOUBLE_TOLERANCE(imageSpacing[i], nodeSpacing[i], 1.0e-4);
    }

    CHECK_NOT_NULL(typedNode->GetImageData()->GetScalarPointer());

  } 
  catch (std::exception& e)
  {
    std::cerr << "Unexpected Exception occurred: " << e.what() << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
