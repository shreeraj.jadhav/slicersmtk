//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Harald Scheirich, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================


// Slicer includes
#include <ctkCoreTestingMacros.h>
#include <vtkMRMLScene.h>
#include <vtkMRMLNode.h>
#include <vtkMRMLLabelMapVolumeNode.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileReaderLogic.h>

// Std includes
#include <iostream>
#include <vector>
#include <algorithm>

int vtkSlicerSMTKFileReaderLogicImportSegmentationSceneTest(int argc, char* argv[])
{
  bool test_success = true;
  std::string fileName = "SegmentationScene.aeva.smtk";
  std::string scratchDir = ".";
  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/";
  if (argc >= 3)
  {
    scratchDir = std::string(argv[2]);
  }

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSMTKFileReaderLogic> logic;

  try
  {
    // Should have one node 
    // Expecting one node that is an model node
    auto importedNodes = logic->ImportMRMLSceneFromAevaSMTKResources(scene, fileName, dataDir);

    CHECK_INT(importedNodes.size(), 3);
    CHECK_INT(scene->GetNumberOfNodes(), 3);
    for (int i = 0; i < 3; ++i)
    {
      auto node = scene->GetNthNode(i);
      CHECK_BOOL(std::find(importedNodes.begin(), importedNodes.end(), std::string(node->GetName())) != importedNodes.end(), true);
      auto typedNode = dynamic_cast<vtkMRMLLabelMapVolumeNode*>(node);
      CHECK_NOT_NULL(typedNode);
    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Unexpected Exception occurred: " << e.what() << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
