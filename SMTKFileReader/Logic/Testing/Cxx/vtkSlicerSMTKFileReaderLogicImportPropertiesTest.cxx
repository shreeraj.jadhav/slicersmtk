//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK includes
#include <vtkImageData.h>
// #include <vtkMatrix3x3.h>

// Slicer includes
#include <vtkMRMLScene.h>
#include <vtkMRMLVolumeNode.h>
#include <vtkSlicerSegmentationsModuleLogic.h>
#include <vtkMRMLLayoutNode.h>
#include <vtkMRMLSegmentationDisplayNode.h>
#include <vtkOrientedImageData.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileReaderLogic.h>
#include "SlicerSMTKCommonTestHelper.hpp"

int vtkSlicerSMTKFileReaderLogicImportPropertiesTest(int argc, char* argv[])
{
  bool test_success = true;
  const std::string inputFilename = "PropertiesScene.aeva.smtk";
  std::string scratchDir = ".";

  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/";
  if (argc >= 3)
  {
    dataDir = std::string(argv[1]) + "/";
    scratchDir = std::string(argv[2]);
  }
  std::cout << "dataDir = " << dataDir << std::endl;

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSegmentationsModuleLogic> segModuleLogic;
  segModuleLogic->SetMRMLScene(scene);

  // Set LayoutDescription for MRML layout nodes in the scene,
  // to avoid warnings that can lead to test failure.
  vtkSmartPointer<vtkCollection> layoutnodes;
  layoutnodes.TakeReference(scene->GetNodesByClass("vtkMRMLLayoutNode"));
  for (int i = 0; i < layoutnodes->GetNumberOfItems(); ++i)
  {
    if (auto node = vtkMRMLLayoutNode::SafeDownCast(layoutnodes->GetItemAsObject(i)))
    {
      int viewarr = node->GetViewArrangement();
      if (!node->IsLayoutDescription(viewarr))
      {
        node->AddLayoutDescription(viewarr, "");
      }
    }
  }

  vtkNew<vtkSlicerSMTKFileReaderLogic> smtkFileReaderLogic;
  smtkFileReaderLogic->ImportMRMLSceneFromAevaSMTKResources(scene, inputFilename, dataDir);

  // RUN LOGIC AND READ OUTPUT:
  auto readResource = readAevaSMTKFile(test_success, inputFilename, dataDir);

  smtk::model::EntityRefs volumes =
      readResource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::VOLUME);

  test_success &= (bool)test(!volumes.empty(), "No volumes.");
  test_success &= (bool)test(volumes.size() == 2, "Unexpected number of volumes.");

  for (auto& v : volumes)
  {
    auto readSession = readResource->session();
    vtkSmartPointer<vtkImageData> volumeImageData =
      vtkImageData::SafeDownCast(readSession->findStorage(v.entity()));

    // Test that we have a valid vtkImageData object (whether a volume or segmentation node).
    test_success &= (bool)test(volumeImageData.Get() != nullptr, "Cannot convert stored volume to vtkImageData.");
    if (!volumeImageData)
    {
      continue;
    }

    const auto aevaDataType = TryGetSMTKPropertyValue<std::string>(v.component(), "aeva_datatype");

    // Tests for volume node
    if (aevaDataType == "image")
    {
      // get the volume node of the scene, this call works since we expect only one node of this type in the input test scene.
      auto volumeNode = vtkMRMLVolumeNode::SafeDownCast(scene->GetFirstNodeByClass("vtkMRMLVolumeNode"));
      test_success &= (bool)test(volumeNode != nullptr, "Unable to get vtkMRMLVolumeNode from the input scene.");
      if (!volumeNode)
      {
        continue;
      }

      double slicerSpacing[3] = { 0, 0, 0 };
      volumeNode->GetSpacing(slicerSpacing);
      double slicerOrigin[3] = { 0, 0, 0 };
      volumeNode->GetOrigin(slicerOrigin);
      double slicerDirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };
      volumeNode->GetIJKToRASDirections(slicerDirs);

      test_success &= testImageDataGeometry(volumeImageData, slicerOrigin, slicerSpacing, slicerDirs);

      // Test numOfPoints and numOfCells in image
      {
        std::ostringstream explanation;
        explanation << "Volume geometry contains an unexpected number of points and/or cells\n"
          << volumeImageData->GetNumberOfPoints() << " " << volumeImageData->GetNumberOfCells();
        test_success &= (bool)test(
          volumeImageData->GetNumberOfPoints() == 100352 &&
          volumeImageData->GetNumberOfCells() == 93555,
          explanation.str());
      }
    }
    // Tests for segmentation node
    else if (aevaDataType == "segmentation")
    {
      // Get the volume node of the scene, this call works since we expect only one onde of this type in the input test scene.
      auto segNode = vtkMRMLSegmentationNode::SafeDownCast(scene->GetFirstNodeByClass("vtkMRMLSegmentationNode"));
      test_success &= (bool)test(segNode != nullptr, "Unable to get vtkMRMLSegmentationNode from the input scene.");
      if (!segNode)
      {
        continue;
      }

      double slicerOrigin[3] = { 0, 0, 0 };
      double slicerSpacing[3] = { 0, 0, 0 };
      double slicerDirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };

      vtkNew<vtkOrientedImageData> labelMap;
      segNode->GenerateMergedLabelmapForAllSegments(labelMap);
      labelMap->GetOrigin(slicerOrigin);
      labelMap->GetSpacing(slicerSpacing);
      labelMap->GetDirections(slicerDirs);

      test_success &= testImageDataGeometry(volumeImageData, slicerOrigin, slicerSpacing, slicerDirs);

      // Test numOfPoints and numOfCells in image
      {
        std::ostringstream explanation;
        explanation << "Segmentation geometry contains an unexpected number of points and/or cells\n"
          << volumeImageData->GetNumberOfPoints() << " " << volumeImageData->GetNumberOfCells();
        test_success &= (bool)test(
          volumeImageData->GetNumberOfPoints() == 17640 &&
          volumeImageData->GetNumberOfCells() == 15456,
          explanation.str());
      }
    }
  }

  // Report the overall success of this test:
  if(test_success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
