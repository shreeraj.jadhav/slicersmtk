//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

#ifndef __SlicerSMTKCommonTestHelper_hpp
#define __SlicerSMTKCommonTestHelper_hpp

// VTK includes
#include <vtkAddonTestingUtilities.h>
#include <vtkImageData.h>
#include <vtkMatrix3x3.h>

// Slicer includes
#include <vtkMRMLScene.h>

// smtk includes
#include <smtk/attribute/Attribute.h>
#include <smtk/attribute/IntItem.h>
#include <smtk/attribute/FileItem.h>
#include <smtk/attribute/ResourceItem.h>
#include <smtk/common/testing/cxx/helpers.h>
#include <smtk/operation/Manager.h>
#include <smtk/operation/Operation.h>

// aeva-session includes
#include <smtk/session/aeva/operators/Read.h>
#include <smtk/session/aeva/Registrar.h>
#include <smtk/session/aeva/Resource.h>

using smtk::session::aeva::Read;

namespace
{

smtk::session::aeva::Resource::Ptr readAevaSMTKFile(
  bool& test_success,
  const std::string& inputFilename,
  const std::string& path)
{
  // TEST THE OUTPUT:
  // Create the aeva-session pipeline to read back the output scene file.
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();
  smtk::session::aeva::Registrar::registerTo(resourceManager);
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();
  smtk::session::aeva::Registrar::registerTo(operationManager);
  operationManager->registerResourceManager(resourceManager);

  // Now try to read the output .aeva.smtk file.
  auto readOp = operationManager->create<Read>();
  test_success &= (bool)test(!!readOp, "No Read operation");
  readOp->parameters()->findFile("filename")->setValue(path + "/" + inputFilename);
  test_success &= (bool)test(readOp->ableToOperate(), "Read operation unable to operate");
  Read::Result result = readOp->operate();

  // Check if operation was successful 
  test_success &= (bool)test(result->findInt("outcome")->value() ==
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED),
    "Read operation failed");

  auto readResource =
      std::dynamic_pointer_cast<smtk::session::aeva::Resource>(
          result->findResource("resource")->value());

  test_success &= (bool)test(!!readResource, "No resource read.");

  return readResource;
}

/**
 * @brief Compare the vtkImageData geometry with the provided expected values. Geometry of the
 * image implies position of the origin, spacing of voxels, and orientation of the grid.
 * This test is designed to test whether the geometry/orientation of segments from segmentation
 * node are equivalent to their source/reference ScalarVolume node. Typically, segments
 * may not have the same dimensions / extents as the source/reference volume, hence they
 * are not compared/tested in this function.
 * @param image - Input image data object (typically, a labelmap that is a representation of a segment)
 * @param expOrigin - Expected value of origin.
 * @param expSpacing - Expected value of spacing.
 * @param expDirs - Expected value of directions/orientation.
 * @return - Returns true if values within input image are equivalent (under tolerance) to the expected
 * values; returns false otherwise.
*/
bool testImageDataGeometry(vtkImageData* image, double expOrigin[3], double expSpacing[3], double expDirs[3][3])
{
  bool test_success = true;

  // Test origin values
  {
    double origin[3] = { 0, 0, 0 };
    image->GetOrigin(origin);

    std::ostringstream explanation;
    explanation << "Volume geometry origin has unexpected values\n"
      << "{ " << origin[0] << ", " << origin[1] << ", " << origin[2] << " } != "
      << "{ " << expOrigin[0] << ", " << expOrigin[1] << ", " << expOrigin[2] << " }";

    test_success &= (bool)test(
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", origin[0], expOrigin[0], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", origin[1], expOrigin[1], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", origin[2], expOrigin[2], 1e-6),
      explanation.str());
  }

  // Test spacing values
  {
    double spacing[3] = { 0, 0, 0 };
    image->GetSpacing(spacing);

    std::ostringstream explanation;
    explanation << "Volume geometry spacing has unexpected values\n"
      << "{ " << spacing[0] << ", " << spacing[1] << ", " << spacing[2] << " } != "
      << "{ " << expSpacing[0] << ", " << expSpacing[1] << ", " << expSpacing[2] << " }";

    test_success &= (bool)test(
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", spacing[0], expSpacing[0], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", spacing[1], expSpacing[1], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", spacing[2], expSpacing[2], 1e-6),
      explanation.str());
  }

  // Test Direction values
  {
    double dirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        dirs[i][j] = image->GetDirectionMatrix()->GetElement(i, j);
      }
    }

    std::ostringstream explanation;
    explanation << "Volume Directions has unexpected values";

    test_success &= (bool)test(
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[0][0], expDirs[0][0], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[0][1], expDirs[0][1], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[0][2], expDirs[0][2], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[1][0], expDirs[1][0], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[1][1], expDirs[1][1], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[1][2], expDirs[1][2], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[2][0], expDirs[2][0], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[2][1], expDirs[2][1], 1e-6) &&
      vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", dirs[2][2], expDirs[2][2], 1e-6),
      explanation.str());
  }

  return test_success;
}

} // end of anonymous namespace

#endif

