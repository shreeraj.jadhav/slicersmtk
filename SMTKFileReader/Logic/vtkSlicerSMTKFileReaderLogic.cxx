//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

/*==========================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==========================================================================*/

// SMTKFileReader includes
#include "vtkSlicerSMTKFileReaderLogic.h"

// VTK includes
#include <vtkImageData.h>
#include <vtkMatrix3x3.h>
#include <vtkOrientedImageData.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLImageDataReader.h>


// MRML includes
#include <vtkMRMLModelDisplayNode.h>
#include <vtkMRMLModelNode.h>
#include <vtkMRMLScalarVolumeNode.h>
#include <vtkMRMLScene.h>
#include <vtkMRMLSelectionNode.h>
#include <vtkMRMLSegmentationNode.h>

// Slicer logic includes
#include <vtkSlicerSegmentationsModuleLogic.h>

// smtk includes
#include <smtk/attribute/FileItem.h>
#include <smtk/attribute/IntItem.h>
#include <smtk/attribute/ResourceItem.h>
#include <smtk/common/Paths.h>
#include <smtk/model/Model.h>
#include <smtk/model/Volume.h>
#include <smtk/resource/Component.h>
#include <smtk/resource/Properties.h>
#include <smtk/resource/Resource.h>

// aeva-session includes
#include <smtk/session/aeva/Predicates.h>
#include <smtk/session/aeva/Registrar.h>
#include <smtk/session/aeva/Resource.h>
#include <smtk/session/aeva/operators/Read.h>

// STD includes
#include <algorithm>
#include <array>
#include <string>
#include <vector>



//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSlicerSMTKFileReaderLogic);

//----------------------------------------------------------------------------
vtkSlicerSMTKFileReaderLogic::vtkSlicerSMTKFileReaderLogic() = default;

//----------------------------------------------------------------------------
vtkSlicerSMTKFileReaderLogic::~vtkSlicerSMTKFileReaderLogic() = default;

//----------------------------------------------------------------------------
void vtkSlicerSMTKFileReaderLogic::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
int vtkSlicerSMTKFileReaderLogic::createMRMLSegmentationNode(
  smtk::resource::ComponentPtr component,
  vtkOrientedImageData* imageFromSMTKScene,
  vtkMRMLScene* mrmlScene,
  const std::string& nameForNewMRMLNode)
{
  int issues = 0;
  auto segNode = dynamic_cast<vtkMRMLSegmentationNode*>(mrmlScene->AddNewNodeByClass(
    "vtkMRMLSegmentationNode", nameForNewMRMLNode));

  if (!vtkSlicerSegmentationsModuleLogic::ImportLabelmapToSegmentationNode(imageFromSMTKScene, segNode)
    || segNode->GetSegmentation() == nullptr)
  {
    vtkErrorMacro("Unable to convert vtkImageData to MRML segmentation node.");
    return ++issues;
  }

  // Copy segmentation attributes from SMTK properties.
  const auto namesArray = TryGetSMTKPropertyValue<std::vector<std::string>>(component, "segmentNames");
  int numOfSegments = segNode->GetSegmentation()->GetNumberOfSegments();
  if (numOfSegments != namesArray.size())
  {
    vtkErrorMacro("Number of segments in the newly constructed segmentation node is expected to match the number of segment IDs read from the smtk json file."
      << "\nNumber of segments in node = " << numOfSegments << "\nNumber of segments read from SMTK property = " << namesArray.size());
    ++issues;
  }

  const auto colorsArray = TryGetSMTKPropertyValue<std::vector<smtk::model::Float>>(component, "colormap");
  if (numOfSegments != colorsArray.size() / 4)
  {
    vtkErrorMacro("Number of segments in the newly constructed segmentation node is expected to match the number of colors read from the smtk json file."
      << "\nNumber of segments in node = " << numOfSegments << "\nNumber of colors read from SMTK property = " << colorsArray.size() / 4);
    ++issues;
  }

  // Set names of segments from SMTK property.
  if (numOfSegments == namesArray.size() && numOfSegments == colorsArray.size() / 4)
  {
    for (size_t i = 0; i < namesArray.size(); ++i)
    {
      auto segment = segNode->GetSegmentation()->GetNthSegment(static_cast<unsigned int>(i));
      segment->SetName(namesArray[i].c_str());
      segment->SetColor(colorsArray[i * 4], colorsArray[i * 4 + 1], colorsArray[i * 4 + 2]);
    }
  }

  m_loadedNodes.push_back(nameForNewMRMLNode);
  return issues;
}

//----------------------------------------------------------------------------
int vtkSlicerSMTKFileReaderLogic::createMRMLScalarVolume(
    vtkOrientedImageData* imageFromSMTKScene,
    vtkMRMLScene* mrmlScene,
    const std::string& nameForNewMRMLNode)
{
  auto volumeNode = dynamic_cast<vtkMRMLScalarVolumeNode*>(mrmlScene->AddNewNodeByClass(
    "vtkMRMLScalarVolumeNode", nameForNewMRMLNode));

  // Copy image geometry information to MRML volume node
  volumeNode->SetOrigin(imageFromSMTKScene->GetOrigin());
  volumeNode->SetSpacing(imageFromSMTKScene->GetSpacing());
  double dirs[3][3];
  imageFromSMTKScene->GetDirections(dirs);
  volumeNode->SetIJKToRASDirections(dirs);

  // Reset image geometry information from the input oriented image, before setting
  // it into the MRML node.
  imageFromSMTKScene->SetOrigin(0, 0, 0);
  imageFromSMTKScene->SetSpacing(1, 1, 1);
  imageFromSMTKScene->SetDirections(1, 0, 0, 0, 1, 0, 0, 0, 1);

  volumeNode->SetAndObserveImageData(imageFromSMTKScene);
  m_loadedNodes.push_back(nameForNewMRMLNode);
  return 0;
}

//----------------------------------------------------------------------------
int vtkSlicerSMTKFileReaderLogic::createMRMLModelVolume(
  smtk::model::CellEntity& cell,
  smtk::session::aeva::SessionPtr session,
  vtkMRMLScene* mrmlScene,
  const std::string& nameForNewMRMLNode)
{
  int issues = 0;
  auto cellComp = cell.component();
  if(auto data = session->findStorage(cellComp->id()))
  {
    auto polyNode = dynamic_cast<vtkMRMLModelNode*>(mrmlScene->AddNewNodeByClass(
      "vtkMRMLModelNode", nameForNewMRMLNode));

    if (auto modelData = vtkUnstructuredGrid::SafeDownCast(data.Get()))
    {
      polyNode->SetAndObserveMesh(modelData);
    }
    else if (auto modelData = vtkPolyData::SafeDownCast(data.Get()))
    {
      polyNode->SetAndObserveMesh(modelData);
    }
    else
    {
      std::string filename = TryGetSMTKPropertyValue<std::string>(cellComp, "filename");
      vtkErrorMacro("vtkSlicerSMTKFileReaderLogic::CopyResourceToScene model storage"
        " should be of type vtkUnstructuredGrid or vtkPolyData <" + filename + ">");
      ++issues;
    }
    this->m_loadedNodes.push_back(polyNode->GetName());
    polyNode->CreateDefaultDisplayNodes();
    vtkMRMLModelDisplayNode* dispNode = polyNode->GetModelDisplayNode();
    smtk::model::FloatList color;
    if (cell.hasColor())
    {
      color = cell.color();
    }
    if (dispNode && color.size() == 4)
    {
      dispNode->SetColor(color[0], color[1], color[2]);
      dispNode->SetOpacity(color[3]);
    }
  }
  else
  {
    std::string filename = TryGetSMTKPropertyValue<std::string>(cellComp, "filename");
    vtkErrorMacro("vtkSlicerSMTKFileReaderLogic::CopyResourceToScene Failed to find"
      " storage for model <" + filename + ">");
    ++issues;
  }

  return issues;
}

//----------------------------------------------------------------------------
// Convert vtkImageData to vtkOrientedImageData
vtkSmartPointer<vtkOrientedImageData> imageDataToOrientedImageData(vtkImageData* imageData)
{
  if (imageData)
  {
    vtkNew<vtkOrientedImageData> oriented;
    oriented->DeepCopy(imageData);

    const double* dmdata = imageData->GetDirectionMatrix()->GetData();
    if (dmdata)
    {
      oriented->SetDirections(
        dmdata[0], dmdata[1], dmdata[2],
        dmdata[3], dmdata[4], dmdata[5],
        dmdata[6], dmdata[7], dmdata[8]
      );
      return oriented;
    }
  }
  return nullptr;
}

//----------------------------------------------------------------------------
int vtkSlicerSMTKFileReaderLogic::CopyResourceToScene(
  vtkMRMLScene* mrmlScene,
  std::shared_ptr<smtk::session::aeva::Resource> resource)
{
  // Note: Modeled after aeva::session::Write see Write::operateInternal()
  smtk::common::UUIDs modelIds = resource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY);
  int issues = 0;

  auto session = resource->session();
  if (session == nullptr) {
    vtkErrorMacro("SMTKFileReaderLogic::CopyResourceToScene: resource->session() returned nullptr");
    return 1;
  }

  std::string fileDirectory = smtk::common::Paths::directory(resource->location()) + "/";

  for (auto const& id : modelIds)
  {
    smtk::model::Model dataset = smtk::model::Model(resource, id);
    auto modelComp = dataset.component();
    std::string aevaDatatype = TryGetSMTKPropertyValue<std::string>(modelComp, "aeva_datatype");

    if (aevaDatatype == "image")
    {
      if (dataset.cells().size() != 1)
      {
        vtkErrorMacro("vtkSlicerSMTKFileReaderLogic::CopyResourceToScene Expected one " <<
          "volume cell in image model, got " << dataset.cells().size());
        ++issues;
        continue;
      }

      auto volume = dataset.cells()[0];
      auto volComp = volume.component();
      auto data = session->findStorage(volume.entity());

      if(auto imageFromSMTKScene = imageDataToOrientedImageData(vtkImageData::SafeDownCast(data.Get())))
      {
        if (TryGetSMTKPropertyValue<std::string>(volComp, "aeva_datatype") == "segmentation")
        {
          issues += createMRMLSegmentationNode(volComp, imageFromSMTKScene, mrmlScene, dataset.name());
        }
        else
        {
          issues += createMRMLScalarVolume(imageFromSMTKScene, mrmlScene, dataset.name());
        }
      }
      else
      {
        std::string filename = TryGetSMTKPropertyValue<std::string>(volComp, "filename");
        vtkErrorMacro("vtkSlicerSMTKFileReaderLogic::CopyResourceToScene Failed to find storage for image <" + filename + ">");
        ++issues;
      }
    }
    else if (aevaDatatype == "poly")
    {
      for (auto cell : dataset.cells())
      {
        issues += createMRMLModelVolume(cell, session, mrmlScene, dataset.name());
      }
    }
    else
    {
      vtkWarningMacro("Unknown aeva datatype encountered: " << aevaDatatype);
      ++issues;
    }
  }
  return issues;
}

//----------------------------------------------------------------------------
std::vector<std::string> vtkSlicerSMTKFileReaderLogic::ImportMRMLSceneFromAevaSMTKResources(
  vtkMRMLScene* scene,
  const std::string& smtkFileName,
  const std::string& smtkImportDir)
{
  this->m_importDir = smtkImportDir;
  this->m_loadedNodes.clear();

  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();
  smtk::session::aeva::Registrar::registerTo(resourceManager);
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();
  smtk::session::aeva::Registrar::registerTo(operationManager);
  operationManager->registerResourceManager(resourceManager);

  vtkDebugMacro("SMTKFileReader: attempting to read " << smtkFileName);
  using smtk::session::aeva::Read;
  auto readOp = operationManager->create<Read>();
  readOp->parameters()->findFile("filename")->setValue(m_importDir + smtkFileName);

  Read::Result result = readOp->operate();
  bool success = result->findInt("outcome")->value() == static_cast<int>(Read::Outcome::SUCCEEDED);

  // Forward smtk log records to Slicer's error log.
  auto logger = readOp->log();

  if (success)
  {
    vtkDebugMacro("STMK File successfully read");
    auto aevaResource = std::dynamic_pointer_cast<smtk::session::aeva::Resource>(result->findResource("resource")->value());
    auto issueCount = this->CopyResourceToScene(scene, aevaResource);
    if (issueCount == 0)
    {
      vtkDebugMacro("SMTK File sucessfully imported into scene");
    }
    else
    {
      vtkDebugMacro("SMTK File imported with " << issueCount << "issue/s, check the log for more information");
    }
  }
  else
  {
    vtkErrorMacro("Failed to import SMTK File into MRML Scene");
  }

  return this->m_loadedNodes;
}
