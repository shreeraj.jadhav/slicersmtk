//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/
// Qt includes
#include <QDebug>

// SlicerQt includes
#include <qSlicerCoreApplication.h>
#include <qSlicerIOManager.h>
#include <qSlicerNodeWriter.h>

// SMTKFileWriter Logic includes
#include <vtkSlicerSMTKFileWriterLogic.h>

// SMTKFileWriter QTModule includes
#include "qSlicerSMTKFileWriterPlugin.h"
#include "qSlicerSMTKFileWriterModule.h"
#include "qSlicerSMTKFileWriterPluginWidget.h"


//-----------------------------------------------------------------------------
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtPlugin>
Q_EXPORT_PLUGIN2(qSlicerSMTKFileWriterModule, qSlicerSMTKFileWriterModule);
#endif

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileWriter
class qSlicerSMTKFileWriterModulePrivate
{
public:
  qSlicerSMTKFileWriterModulePrivate();
};

//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterModulePrivate::qSlicerSMTKFileWriterModulePrivate() = default;


//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterModule::qSlicerSMTKFileWriterModule(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileWriterModulePrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterModule::~qSlicerSMTKFileWriterModule() = default;

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileWriterModule::helpText()const
{
  QString help = QString(
    "The SMTKFileWriter module enables exporting Slicer MRML nodes an aeva SMTK (.smtk) resource file.<br>");
  return help;
}

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileWriterModule::acknowledgementText()const
{
  QString acknowledgement = QString(
    "This work is part of the AEVA project");
  return acknowledgement;
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileWriterModule::contributors()const
{
  QStringList moduleContributors;
  moduleContributors << QString("Andinet Enquobahrie");
  return moduleContributors;
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileWriterModule::categories()const
{
  return QStringList() << "smtk IO";
}

//-----------------------------------------------------------------------------
void qSlicerSMTKFileWriterModule::setup()
{
  this->Superclass::setup();
  
  vtkSlicerSMTKFileWriterLogic* SMTKFileWriterLogic =  
    vtkSlicerSMTKFileWriterLogic::SafeDownCast(this->logic());

  // Adds the module to the IO Manager
  qSlicerCoreIOManager* ioManager =
    qSlicerCoreApplication::application()->coreIOManager();
  ioManager->registerIO(new qSlicerSMTKFileWriterPlugin(SMTKFileWriterLogic,this));
}

//-----------------------------------------------------------------------------
qSlicerAbstractModuleRepresentation* qSlicerSMTKFileWriterModule::createWidgetRepresentation()
{
  return new qSlicerSMTKFileWriterPluginWidget;
}

//-----------------------------------------------------------------------------
vtkMRMLAbstractLogic* qSlicerSMTKFileWriterModule::createLogic()
{
  return vtkSlicerSMTKFileWriterLogic::New();
}

