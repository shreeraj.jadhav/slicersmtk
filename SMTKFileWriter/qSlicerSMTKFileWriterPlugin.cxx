//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/
// CTK includes
#include <ctkMessageBox.h>

// Qt includes
#include <QDebug>
#include <QFileInfo>
#include <QString>
#include <QStringList>

// QtCore includes
#include "qMRMLUtils.h"
#include "qSlicerCoreApplication.h"
#include "vtkSlicerApplicationLogic.h"

// SlicerQt includes
#include "qSlicerSMTKFileWriterPlugin.h"
#include "qSlicerSMTKFileWriterOptionsWidget.h"

// Logic includes
#include "vtkSlicerSMTKFileWriterLogic.h"

// MRML includes
#include <vtkMRMLMessageCollection.h>
#include <vtkMRMLScene.h>

//-----------------------------------------------------------------------------
/// \ingroup SlicerRt_QtModules_SMTKFileWriter
class qSlicerSMTKFileWriterPluginPrivate
{
  public:
  vtkSmartPointer<vtkSlicerSMTKFileWriterLogic> Logic;
};

//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterPlugin::qSlicerSMTKFileWriterPlugin(QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileWriterPluginPrivate)
{
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterPlugin::qSlicerSMTKFileWriterPlugin(vtkSlicerSMTKFileWriterLogic* logic, QObject* _parent)
  : Superclass(_parent)
  , d_ptr(new qSlicerSMTKFileWriterPluginPrivate)
{
  this->setLogic(logic);
}

//-----------------------------------------------------------------------------
qSlicerSMTKFileWriterPlugin::~qSlicerSMTKFileWriterPlugin() = default;

//-----------------------------------------------------------------------------
void qSlicerSMTKFileWriterPlugin::setLogic(vtkSlicerSMTKFileWriterLogic* logic)
{
  Q_D(qSlicerSMTKFileWriterPlugin);
  d->Logic = logic;
}

//-----------------------------------------------------------------------------
vtkSlicerSMTKFileWriterLogic* qSlicerSMTKFileWriterPlugin::logic()const
{
  Q_D(const qSlicerSMTKFileWriterPlugin);
  return d->Logic.GetPointer();
}

//-----------------------------------------------------------------------------
QString qSlicerSMTKFileWriterPlugin::description()const
{
  return "SMTK";
}

//-----------------------------------------------------------------------------
qSlicerIO::IOFileType qSlicerSMTKFileWriterPlugin::fileType()const
{
  // return QString("SMTK File");
  return QString("SceneFile");
}

//-----------------------------------------------------------------------------
QStringList qSlicerSMTKFileWriterPlugin::extensions(vtkObject* object)const
{
  Q_UNUSED(object);

  return QStringList() << "aeva SMTK (*.aeva.smtk)";
}

//----------------------------------------------------------------------------
bool qSlicerSMTKFileWriterPlugin::canWriteObject(vtkObject* object)const
{
    return vtkMRMLScene::SafeDownCast(object);
}

//----------------------------------------------------------------------------
bool qSlicerSMTKFileWriterPlugin::write(const qSlicerIO::IOProperties& properties)
{
  this->setWrittenNodes(QStringList());

  Q_ASSERT(!properties["fileName"].toString().isEmpty());
  QFileInfo fileInfo(properties["fileName"].toString());
  QString baseDir = fileInfo.absolutePath();
  if (!QFileInfo(baseDir).isWritable())
  {
    qWarning() << "Failed to save" << fileInfo.absoluteFilePath() << ":"
        << "Path" << baseDir << "is not writable";
    this->userMessages()->AddMessage(vtkCommand::ErrorEvent,
        tr("Failed to save scene as %1 (path %2 is not writeable)").arg(fileInfo.absoluteFilePath()).arg(baseDir).toStdString());
    return 0;
  }
  bool res = false;
  if (fileInfo.suffix() == "smtk")
  {
    res = this->writeToAevaSMTK(properties);
  }
  else
  {
    qWarning() << "Cannot handle the file extension: " << fileInfo.suffix();
  }

  return res;
}

//----------------------------------------------------------------------------
bool qSlicerSMTKFileWriterPlugin::writeToAevaSMTK(const qSlicerIO::IOProperties& properties)
{
  QFileInfo fileInfo(properties["fileName"].toString());
  QString fullPath = fileInfo.absoluteFilePath();

  std::string smtkFileName = fileInfo.fileName().toStdString();
  std::string smtkExportDir = fileInfo.absolutePath().toStdString();
  return logic()->ExportMRMLSceneToAevaSMTKResources(this->mrmlScene(), smtkFileName, smtkExportDir);
}
