//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

/*==============================================================================

  Copyright (c) Laboratory for Percutaneous Surgery (PerkLab)
  Queen's University, Kingston, ON, Canada. All Rights Reserved.

  See COPYRIGHT.txt
  or http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Jennifer Andrea, PerkLab, Queen's University
  and was supported through the Applied Cancer Research Unit program of Cancer Care
  Ontario with funds provided by the Natural Sciences and Engineering Research Council
  of Canada.

==============================================================================*/

#ifndef __qSlicerSMTKFileWriterOptionsWidget_h
#define __qSlicerSMTKFileWriterOptionsWidget_h

// CTK includes
#include <ctkPimpl.h>

// SlicerQt includes
#include "qSlicerIOOptionsWidget.h"

// SMTKFileWriter includes
#include "qSlicerSMTKFileWriterModuleExport.h"

class qSlicerSMTKFileWriterOptionsWidgetPrivate;

/// \ingroup SlicerRt_QtModules_SMTKFileWriter
class Q_SLICER_SMTKFILEWRITER_EXPORT qSlicerSMTKFileWriterOptionsWidget :
  public qSlicerIOOptionsWidget
{
  Q_OBJECT
public:
  typedef qSlicerIOOptionsWidget Superclass;
  qSlicerSMTKFileWriterOptionsWidget(QWidget *parent=nullptr);
  ~qSlicerSMTKFileWriterOptionsWidget() override;


protected slots:
  void updateProperties();

private:
  Q_DECLARE_PRIVATE_D(qGetPtrHelper(qSlicerIOOptions::d_ptr), qSlicerSMTKFileWriterOptionsWidget);
  //Q_DECLARE_PRIVATE(qSlicerSMTKFileWriterOptionsWidget);
  Q_DISABLE_COPY(qSlicerSMTKFileWriterOptionsWidget);
};

#endif
