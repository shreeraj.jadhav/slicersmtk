set(KIT ${PROJECT_NAME})

#-----------------------------------------------------------------------------
set(KIT_TEST_SRCS
  ${PROJECT_NAME}ExportEmptySceneTest.cxx
  ${PROJECT_NAME}ExportImageDataTest.cxx
  ${PROJECT_NAME}ExportPolyDataTest.cxx
  ${PROJECT_NAME}ExportPropertiesTest.cxx
  ${PROJECT_NAME}ExportSegmentationTest.cxx
  )

#-----------------------------------------------------------------------------
slicerMacroConfigureModuleCxxTestDriver(
  NAME ${KIT}
  SOURCES ${KIT_TEST_SRCS}
  WITH_VTK_DEBUG_LEAKS_CHECK
  WITH_VTK_ERROR_OUTPUT_CHECK
  )

#-----------------------------------------------------------------------------
set(SlicerSMTK_DATA_FOLDER ${Slicer_BINARY_DIR}/../SlicerSMTK/Data/Testing)
set(SlicerSMTK_MRML_DATA_FOLDER ${Slicer_BINARY_DIR}/Libs/MRML/Core/Testing/TestData)
set(SlicerSMTK_SCRATCH_FOLDER ${Slicer_BINARY_DIR}/Testing/Temporary)

simple_test(${PROJECT_NAME}ExportEmptySceneTest ${Slicer_BINARY_DIR}/../aeva-session/data ${SlicerSMTK_SCRATCH_FOLDER})
simple_test(${PROJECT_NAME}ExportImageDataTest ${SlicerSMTK_DATA_FOLDER}/Volumes ${SlicerSMTK_SCRATCH_FOLDER})
simple_test(${PROJECT_NAME}ExportPolyDataTest ${Slicer_BINARY_DIR}/../aeva-session/data ${SlicerSMTK_SCRATCH_FOLDER})
simple_test(${PROJECT_NAME}ExportPropertiesTest ${SlicerSMTK_DATA_FOLDER}/PropertiesMRMLScene ${SlicerSMTK_SCRATCH_FOLDER})
simple_test(${PROJECT_NAME}ExportSegmentationTest ${SlicerSMTK_MRML_DATA_FOLDER} ${SlicerSMTK_SCRATCH_FOLDER})

