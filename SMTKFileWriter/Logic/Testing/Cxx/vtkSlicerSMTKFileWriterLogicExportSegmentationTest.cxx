//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK  includes
#include <vtkImageData.h>

// Slicer includes
#include <vtkMRMLScene.h>
#include <vtkMRMLSegmentationNode.h>
#include <vtkSlicerSegmentationsModuleLogic.h>

// smtk includes
#include <smtk/model/Volume.h>

// SlicerSMTK includes
#include "TestHelper.hpp"

int vtkSlicerSMTKFileWriterLogicExportSegmentationTest(int argc, char* argv[])
{
  const std::string outputFilename = "SegmentationScene.aeva.smtk";
  const std::string inputFilename = "SlicerSegmentation.seg.nrrd";
  constexpr size_t expectedNumSegments = 2;
  const std::set<vtkIdType> expectedNumPoints = { 291525, 389480 };
  const std::set<vtkIdType> expectedNumCells = { 278528, 373120 };

  bool test_success = true;
  std::string scratchDir = ".";

  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../Libs/MRML/Core/Testing/TestData";

  if (argc >= 3)
  {
    dataDir = std::string(argv[1]);
    scratchDir = std::string(argv[2]);
  }

  // PREPARE INPUT:
  vtkNew<vtkMRMLScene> scene;
  // Read input segmentation file to MRML node and scene.
  vtkNew<vtkSlicerSegmentationsModuleLogic> segLogic;
  segLogic->SetMRMLScene(scene);
  auto node = segLogic->LoadSegmentationFromFile(std::string(dataDir + "/" + inputFilename).c_str());
  test_success &= (bool)test(!!node, "Reading input segmentation file failed.");

  // RUN LOGIC AND READ OUTPUT:
  auto readResource = testExportSceneToAevaSMTKFile(test_success, scene, outputFilename, scratchDir);

  smtk::model::EntityRefs volumes =
      readResource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::VOLUME);

  test_success &= (bool)test(!volumes.empty(), "No volumes.");
  test_success &= (bool)test((volumes.size() == expectedNumSegments), "Exceeded the expected number of segments.");

  // Access and check all of segmentation volumes:
  auto readSession = readResource->session();
  for (smtk::model::Volume volume : volumes)
  {
    std::cout << "Volume is " << volume.name() << " " << volume.entity() << "\n";

    vtkSmartPointer<vtkImageData> volumeImageData =
    vtkImageData::SafeDownCast(readSession->findStorage(volume.entity()));

    test_success &= (bool)test(!!volumeImageData.Get(), "Cannot convert stored volume to vtkImageData.");

    if (volumeImageData.Get())
    {
      std::ostringstream pointExplanation, cellExplanation;
      pointExplanation << "Volume geometry contains an unexpected number of points: " << volumeImageData->GetNumberOfPoints();
      cellExplanation << "Volume geometry contains an unexpected number of cells: " << volumeImageData->GetNumberOfCells();
      test_success &= (bool)test(expectedNumPoints.count(volumeImageData->GetNumberOfPoints()) == 1, "Doesn't have the expected number of points.");
      test_success &= (bool)test(expectedNumCells.count(volumeImageData->GetNumberOfCells()) == 1, "Doesn't have the expected number of cells.");
    }
  }

  // report the overall success of this test:
  if(test_success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
