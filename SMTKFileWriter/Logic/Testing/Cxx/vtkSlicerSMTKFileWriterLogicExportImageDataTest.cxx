//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK  includes
#include <vtkAddonTestingUtilities.h>
#include <vtkImageData.h>
#include <vtkMatrix3x3.h>

// Slicer includes
#include <vtkMRMLColorLogic.h>
#include <vtkMRMLScene.h>
#include <vtkSlicerVolumesLogic.h>

// smtk includes
#include <smtk/model/Volume.h>

// SlicerSMTK includes
#include "TestHelper.hpp"

int vtkSlicerSMTKFileWriterLogicExportImageDataTest(int argc, char* argv[])
{
  const std::string outputFilename = "ImageScene.aeva.smtk";
  const std::string inputFilename = "/transformed_image.nii";

  bool test_success = true;
  std::string scratchDir = ".";

  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/Volumes";
  if (argc >= 3)
  {
    dataDir = std::string(argv[1]);
    scratchDir = std::string(argv[2]);
  }

  // Use Slicer's logic to load the image data instead of directly reading using a VTK reader class.
  // This ensures proper testing of all properties that are loaded by Slicer.
  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkMRMLApplicationLogic> appLogic;

  // vtkMRMLColorLogic is not needed for the test, but added here to avoid a console warning
  // during the call to vtkSlicerVolumesLogic::AddArchetypeVolume() inside vtkSlicerVolumesLogic::SetAndObserveColorToDisplayNode().
  vtkNew<vtkMRMLColorLogic> colorLogic;
  appLogic->SetModuleLogic("Colors", colorLogic);

  vtkNew<vtkSlicerVolumesLogic> volumesLogic;
  volumesLogic->SetMRMLScene(scene);
  volumesLogic->SetMRMLApplicationLogic(appLogic);
  auto volumeNode = volumesLogic->AddArchetypeVolume(std::string(dataDir + inputFilename).c_str(), "testVolume");

  // RUN LOGIC AND READ OUTPUT:
  auto readResource = testExportSceneToAevaSMTKFile(test_success, scene, outputFilename, scratchDir);

  smtk::model::EntityRefs volumes =
      readResource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::VOLUME);

  test_success &= (bool)test(!volumes.empty(), "No volumes.");
  test_success &= (bool)test(volumes.size() == 1, "Unexpected number of volumes.");

  smtk::model::Volume volume = *volumes.begin();
  std::cout << "Volume is " << volume.name() << " " << volume.entity() << "\n";
  auto readSession = readResource->session();
  vtkSmartPointer<vtkImageData> volumeImageData =
    vtkImageData::SafeDownCast(readSession->findStorage(volume.entity()));

  test_success &= (bool)test(!!volumeImageData.Get(), "Cannot convert stored volume to vtkImageData.");

  if (volumeImageData.Get())
  {
    // Test numOfPoints and numOfCells in image
    {
      std::ostringstream explanation;
      explanation << "Volume geometry contains an unexpected number of points and/or cells\n"
        << volumeImageData->GetNumberOfPoints() << " " << volumeImageData->GetNumberOfCells();
      test_success &= (bool)test(
        volumeImageData->GetNumberOfPoints() == 100352 &&
        volumeImageData->GetNumberOfCells() == 93555,
        explanation.str());
    }

    double slicerOrigin[3] = { 0, 0, 0 };
    double slicerSpacing[3] = { 0, 0, 0 };
    double slicerDirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };

    volumeNode->GetOrigin(slicerOrigin);
    volumeNode->GetSpacing(slicerSpacing);
    volumeNode->GetIJKToRASDirections(slicerDirs);

    test_success &= testImageDataGeometry(volumeImageData, slicerOrigin, slicerSpacing, slicerDirs);
  }

  // report the overall success of this test:
  if(test_success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
