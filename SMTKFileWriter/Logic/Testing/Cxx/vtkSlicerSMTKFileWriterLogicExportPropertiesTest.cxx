//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  This file was developed by Shreeraj Jadhav, Kitware, Inc. based
//  on templates generated using SlicerExtensionWizard & SlicerCustomAppTemplate,
//  and was partially funded by NIH grant R01EB025212.
//=========================================================================

// VTK includes
#include <vtkAddonTestingUtilities.h>
#include <vtkImageData.h>
#include <vtkMatrix3x3.h>

// Slicer includes
#include <vtkMRMLModelDisplayNode.h>
#include <vtkMRMLModelNode.h>
#include <vtkMRMLScene.h>
#include <vtkMRMLVolumeNode.h>
#include <vtkSegmentationConverterFactory.h>
#include <vtkSlicerSegmentationsModuleLogic.h>
#include <vtkMRMLLayoutNode.h>

// SlicerSMTK includes
#include <vtkSlicerSMTKFileReaderLogic.h>
#include <vtkSlicerSMTKFileWriterLogic.h>
#include "TestHelper.hpp"

int vtkSlicerSMTKFileWriterLogicExportPropertiesTest(int argc, char* argv[])
{
  bool test_success = true;
  const std::string inputFilename = "Properties.mrml";
  const std::string outputFilename = "Properties.aeva.smtk";

  std::string scratchDir = ".";

  // Default dataDir path for debugging the test.
  // Actual test-run will set this path through command line argument.
  // Check the CMakeLists file to see what args are passed.
  std::string dataDir = "../../../../../../../SlicerSMTK/Data/Testing/PropertiesMRMLScene";
  if (argc >= 3)
  {
    dataDir = std::string(argv[1]);
    scratchDir = std::string(argv[2]);
  }
  std::cout << "dataDir: " << dataDir << std::endl;

  vtkNew<vtkMRMLScene> scene;
  vtkNew<vtkSlicerSegmentationsModuleLogic> segModuleLogic;
  segModuleLogic->SetMRMLScene(scene);
  scene->SetURL((dataDir + "/" + inputFilename).c_str());
  scene->Connect();

  // Set LayoutDescription for MRML layout nodes in the scene,
  // to avoid warnings that can lead to test failure.
  vtkSmartPointer<vtkCollection> layoutnodes;
  layoutnodes.TakeReference(scene->GetNodesByClass("vtkMRMLLayoutNode"));
  for (int i = 0; i < layoutnodes->GetNumberOfItems(); ++i)
  {
    if (auto node = vtkMRMLLayoutNode::SafeDownCast(layoutnodes->GetItemAsObject(i)))
    {
      int viewarr = node->GetViewArrangement();
      if (!node->IsLayoutDescription(viewarr))
      {
        node->AddLayoutDescription(viewarr, "");
      }
    }
  }

  // Write out the aeva SMTK scene
  vtkNew<vtkSlicerSMTKFileWriterLogic> smtkFileWriterLogic;
  test_success &= (bool)test(smtkFileWriterLogic->ExportMRMLSceneToAevaSMTKResources(scene, outputFilename, scratchDir), "Export aeva smtk scene file failed.");

  // RUN LOGIC AND READ OUTPUT:
  auto readResource = testExportSceneToAevaSMTKFile(test_success, scene, outputFilename, scratchDir);

  smtk::model::EntityRefs volumes =
      readResource->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::VOLUME);

  test_success &= (bool)test(!volumes.empty(), "No volumes.");
  test_success &= (bool)test(volumes.size() == 2, "Unexpected number of volumes.");

  for (auto& v : volumes)
  {
    auto readSession = readResource->session();
    vtkSmartPointer<vtkImageData> volumeImageData =
      vtkImageData::SafeDownCast(readSession->findStorage(v.entity()));

    // Test that we have a valid vtkImageData object (whether a volume or segmentation node).
    test_success &= (bool)test(volumeImageData.Get() != nullptr, "Cannot convert stored volume to vtkImageData.");
    if (!volumeImageData)
    {
      continue;
    }

    const auto aevaDataType = TryGetSMTKPropertyValue<std::string>(v.component(), "aeva_datatype");

    // Tests for volume node
    if (aevaDataType == "image")
    {
      // get the volume node of the scene, this call works since we expect only one onde of this type in the input test scene.
      auto volumeNode = vtkMRMLVolumeNode::SafeDownCast(scene->GetFirstNodeByClass("vtkMRMLVolumeNode"));
      test_success &= (bool)test(volumeNode != nullptr, "Unable to get vtkMRMLVolumeNode from the input scene.");
      if (!volumeNode)
      {
        continue;
      }

      double slicerSpacing[3] = { 0, 0, 0 };
      volumeNode->GetSpacing(slicerSpacing);
      double slicerOrigin[3] = { 0, 0, 0 };
      volumeNode->GetOrigin(slicerOrigin);
      double slicerDirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };
      volumeNode->GetIJKToRASDirections(slicerDirs);

      test_success &= testImageDataGeometry(volumeImageData, slicerOrigin, slicerSpacing, slicerDirs);

      // Test numOfPoints and numOfCells in image
      {
        std::ostringstream explanation;
        explanation << "Volume geometry contains an unexpected number of points and/or cells\n"
          << volumeImageData->GetNumberOfPoints() << " " << volumeImageData->GetNumberOfCells();
        test_success &= (bool)test(
          volumeImageData->GetNumberOfPoints() == 100352 &&
          volumeImageData->GetNumberOfCells() == 93555,
          explanation.str());
      }
    }
    // Tests for segmentation node
    else if (aevaDataType == "segmentation")
    {
      // get the volume node of the scene, this call works since we expect only one onde of this type in the input test scene.
      auto segNode = vtkMRMLSegmentationNode::SafeDownCast(scene->GetFirstNodeByClass("vtkMRMLSegmentationNode"));
      test_success &= (bool)test(segNode != nullptr, "Unable to get vtkMRMLSegmentationNode from the input scene.");
      if (!segNode)
      {
        continue;
      }
      auto segmentation = segNode->GetSegmentation();
      test_success &= (bool)test(segmentation != nullptr, "Unable to get segmentation out of the vtkMRMLSegmentationNode from the input scene.");
      if (!segmentation)
      {
        continue;
      }

      auto referenceGeometryID = segNode->GetNodeReferenceID(vtkMRMLSegmentationNode::GetReferenceImageGeometryReferenceRole().c_str());
      auto refGeometry = vtkMRMLVolumeNode::SafeDownCast(scene->GetNodeByID(referenceGeometryID));
      double slicerSpacing[3] = { 0, 0, 0 };
      refGeometry->GetSpacing(slicerSpacing);
      double slicerOrigin[3] = { 0, 0, 0 };
      refGeometry->GetOrigin(slicerOrigin);
      double slicerDirs[3][3] = { {0, 0, 0},{0, 0, 0}, {0, 0, 0} };
      refGeometry->GetIJKToRASDirections(slicerDirs);
      test_success &= testImageDataGeometry(volumeImageData, slicerOrigin, slicerSpacing, slicerDirs);

      // Test numOfPoints and numOfCells in image
      {
        std::ostringstream explanation;
        explanation << "Segmentation geometry contains an unexpected number of points and/or cells\n"
          << volumeImageData->GetNumberOfPoints() << " " << volumeImageData->GetNumberOfCells();
        test_success &= (bool)test(
          volumeImageData->GetNumberOfPoints() == 17640 &&
          volumeImageData->GetNumberOfCells() == 15456,
          explanation.str());
      }

      // Test names and colors attributes for segments
      auto namesArray = TryGetSMTKPropertyValue<std::vector<std::string>>(v.component(), "segmentNames");
      int numOfSegs = segmentation->GetNumberOfSegments();
      test_success &= (bool)test(numOfSegs == namesArray.size(), "Number of segments in mrml scene should match namesArray size from aeva smtk property.");
      for (int i = 0; i < numOfSegs; ++i)
      {
        std::string mrmlSegName(segmentation->GetNthSegment(i)->GetName());
        test_success &= (bool)test(mrmlSegName == namesArray[i], "Segment name mismatch.");
      }

      auto colorsArray = TryGetSMTKPropertyValue<std::vector<smtk::model::Float>>(v.component(), "colormap");
      test_success &= (bool)test(numOfSegs == colorsArray.size() / 4, "Number of segments in mrml scene should match colors array size from aeva smtk property.");
      for (int i = 0; i < numOfSegs; ++i)
      {
        double c[] = { 0,0,0 };
        auto s = segmentation->GetNthSegment(i);
        s->GetColor(c);
        std::ostringstream explanation;
        explanation << "Colors value mismatch for segment: " << i << ", name: " << s->GetName();
        test_success &= (bool)test(
          vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", c[0], colorsArray[i * 4 + 0], 1e-6) &&
          vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", c[1], colorsArray[i * 4 + 1], 1e-6) &&
          vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", c[2], colorsArray[i * 4 + 2], 1e-6),
          explanation.str());
      }
    }
  }

  // Test names and colors attributes for models
  smtk::common::UUIDs modelIds = readResource->entitiesMatchingFlags(smtk::model::MODEL_ENTITY);
  int polyCount = 0;
  for (auto const& id : modelIds)
  {
    smtk::model::Model currModel = smtk::model::Model(readResource, id);
    const auto aevaDataType = TryGetSMTKPropertyValue<std::string>(currModel.component(), "aeva_datatype");
    if (aevaDataType == "poly")
    {
      ++polyCount;
      auto modelName = currModel.name();
      test_success &= (bool)test(currModel.cells().size() == 1, "Expected number of cells in model is 1.");
      auto currCell = currModel.cells()[0];
      test_success &= (bool)test(currCell.isValid(), "Invalid cell.");
      if (currCell.isValid())
      {
        auto modelNode = vtkMRMLModelNode::SafeDownCast(scene->GetFirstNodeByName(modelName.c_str()));
        test_success &= (bool)test(modelNode != nullptr, "Model node corresponding to name not found.");
        if (modelNode)
        {
          vtkMRMLModelDisplayNode* dispNode = modelNode->GetModelDisplayNode();
          test_success &= (bool)test(dispNode != nullptr, "Model display node cannot be null.");
          double mrmlNodeColor[] = { 0,0,0 };
          dispNode->GetColor(mrmlNodeColor);
          auto smtkColor = currCell.color();
          std::ostringstream explanation;
          explanation << "Colors value mismatch for model name: " << modelName.c_str();
          test_success &= (bool)test(
            vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", mrmlNodeColor[0], smtkColor[0], 1e-6) &&
            vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", mrmlNodeColor[1], smtkColor[1], 1e-6) &&
            vtkAddonTestingUtilities::CheckDoubleTolerance(__LINE__, "", mrmlNodeColor[2], smtkColor[2], 1e-6),
            explanation.str());
        }
      }
    }
  }
  test_success &= (bool)test(polyCount == 2, "Expected number of Model node is 2.");

  // report the overall success of this test:
  if(test_success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
