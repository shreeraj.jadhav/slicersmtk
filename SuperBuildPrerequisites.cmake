if(DEFINED slicersources_SOURCE_DIR AND NOT DEFINED Slicer_SOURCE_DIR)
  # Explicitly setting "Slicer_SOURCE_DIR" when only "slicersources_SOURCE_DIR"
  # is defined is required to successfully complete configuration in an empty
  # build directory
  #
  # Indeed, in that case, Slicer sources have been downloaded by they have not been
  # added using "add_subdirectory()" and the variable "Slicer_SOURCE_DIR" is not yet in
  # in the CACHE.
  set(Slicer_SOURCE_DIR ${slicersources_SOURCE_DIR})
endif()

# List of <external-project> dependencies used
# (1) to update "<extension-name>_EXTERNAL_PROJECT_DEPENDENCIES" below
# (2) to setup associated "External_<external-project>.cmake"
set(smtk_EXTERNAL_PROJECT_DEPENDENCIES
  Boost
  MOAB
  nlohmann_json
  pegtl
  )
set(aeva-session_EXTERNAL_PROJECT_DEPENDENCIES
  Netgen
  smtk
  )

set(SlicerSMTK_EXTERNAL_PROJECT_DEPENDENCIES
  aeva-session
  smtk
  )

if(DEFINED Slicer_SOURCE_DIR)
  # Extension is bundled in a custom application

  list(APPEND smtk_EXTERNAL_PROJECT_DEPENDENCIES
    LibArchive
    VTK
    )

  list(APPEND aeva-session_EXTERNAL_PROJECT_DEPENDENCIES
    ITK
    VTK
    )

  # Explicit list of dependencies ensuring the custom application bundling this
  # extension does NOT attempt to build external projects associated with VTK modules
  # enabled below.
  list(APPEND SlicerSMTK_EXTERNAL_PROJECT_DEPENDENCIES
    ${smtk_EXTERNAL_PROJECT_DEPENDENCIES}
    ${aeva-session_EXTERNAL_PROJECT_DEPENDENCIES}
    )
  message(STATUS "SlicerSMTK_EXTERNAL_PROJECT_DEPENDENCIES:${SlicerSMTK_EXTERNAL_PROJECT_DEPENDENCIES}")
endif()

if(NOT DEFINED Slicer_SOURCE_DIR)
  # If extension is built standalone, VTKExternalModule is required
  # to configure vtkRenderingExternal and vtkRenderingOpenVR external
  # projects.
  include(${SlicerSMTK_SOURCE_DIR}/FetchVTKExternalModule.cmake)
endif()

if(DEFINED Slicer_SOURCE_DIR)
  # Extension is bundled in a custom application

  # Additional external project options

  # Required by aeva-session
  set(VTK_MODULE_ENABLE_VTK_AcceleratorsVTKmFilters YES)
  set(VTK_MODULE_ENABLE_VTK_FiltersParallelDIY2 YES)
  # Required by smtk
  set(VTK_MODULE_ENABLE_VTK_IOParallelExodus YES)

  mark_as_superbuild(
    VARS
      # Required by aeva-session
      VTK_MODULE_ENABLE_VTK_AcceleratorsVTKmFilters:STRING
      VTK_MODULE_ENABLE_VTK_FiltersParallelDIY2:STRING
      # Required by smtk
      VTK_MODULE_ENABLE_VTK_IOParallelExodus:STRING
    PROJECTS
      VTK
    )

endif()
